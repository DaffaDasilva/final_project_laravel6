<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{
    protected $table = "product";
    protected $fillable =
        [
            'Name',
            'Product_Image',
            'Product_Price',
            'Stock_Product',
            'Description',
            'User_id',
            'created_at',
            'updated_at',
            'Category_id'
        ];

    public function cat_sell() {
        return $this->belongsTo('App\Category','Category_id');
    
    }

     public function user_sell() {
        return $this->belongsTo('App\User','User_id');
    
    }
}
