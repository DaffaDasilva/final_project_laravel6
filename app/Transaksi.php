<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table="transaksi";
    protected $fillable = [
        'Order_Date',
        'Shipping&Payment_Method',
        'Invoice_Amount',
        'created_at',
        'updated_at',
        'beli_id'
    ];

    public function trans_beli() {
        return $this->belongsTo('App\Beli','beli_id');
    
    }
}
