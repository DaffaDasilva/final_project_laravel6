<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Sell;
use App\Beli;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Session;
use Alert;

class ProfileController extends Controller
{
    public function index() {
        $sell = Sell::where('User_id', Auth::user()->id)->with('cat_sell', 'user_sell')->get();
        $status_pembelian = '2';
        $user_id = Auth::user()->id;

        $relasicategory =  Sell::with('cat_sell', 'user_sell')->get();
        $buy = Beli::where([
                ['users_id',$user_id],
                ['Status_Pembelian_id',$status_pembelian]
            ])->with('sell_beli')->get();

        $saleHistory = DB::select('SELECT category.name as Category_name, 
                            transaksi.Order_Date, 
                            transaksi.Invoice_Amount,
                            transaksi.`Shipping&Payment_Method` as status_paid, 
                            beli.Jumlah_Product, product.Name as Product_name 
                            FROM transaksi
                            JOIN beli ON beli.id = transaksi.beli_id
                            JOIN product ON product.id = beli.Product_id
                            JOIN category ON category.id = product.Category_id
                            WHERE product.User_id = \''.Auth::user()->id.'\' ');

        return view('profil', compact('sell','buy','relasicategory', 'saleHistory'));
    }

    public function topUpSaldo(request $request) {
        $request->validate([
            "Saldo" => 'required',
            "password" => 'required'
         ]);

        try{
            $saldo = Auth::user()->Saldo + $request['Saldo'];
            $password = $request['password'];

            if (Hash::check($password, Auth::user()->password)) {
                // Jika password benar
                $user = User::where('id', Auth::user()->id)->update([
                    'Saldo' => $saldo
                ]);
                Alert::success('Success', 'Success Top Up :: Rp.'.$saldo.',-');
            }else{
                // jika password tidak sesuai
                Alert::error('Error', 'Authentication Error.');
            }

        } catch(QueryException $a) {
            $message = $a->getMeessage();
        }
        return redirect('/profile');
    }

    function edit($id){
        $update = User::find($id);
        return view('edit', compact('update'));
    }

    function updateProfile($id, Request $request)
    {
        $imageName = $request['name'].'-'.time().'.'.$request['UsersPhoto']->extension();
        $request['UsersPhoto']->move(public_path('images/foto-profil'), $imageName);

        try{
            $user= User::where('id', $id)->update([
                'name' => $request['name'],
                'username' => $request['username'],
                'UsersPhoto' => $imageName,
                'Alamat' => $request['Alamat'],
                'email' => $request['email'],
                'password' => Hash::make($request['password'])
            ]);
            Alert::success('Success', 'Success Update Profile :: '.$request['Name']);
        } catch(QueryException $ex){
            $ex->getMessage();
            Alert::error('Error', 'Message :: '.$ex);
        }
        
        return redirect('/profile');
    }

    public function purchaseHistory($id, request $request) {
        $product_id = $request->id;
        $status_pembelian = '2';
        $user_id = Auth::user()->id;

        $buy = Beli::where([
                ['users_id',$user_id],
                ['Status_Pembelian_id',$status_pembelian]
            ])->with('sell_beli', 'user_beli')->get();

        return view('profil', ['buy' => $buy]);
    }

}
