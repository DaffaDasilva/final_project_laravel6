<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Sell;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Alert;

class SellController extends Controller
{
    public function create() {
        $category = Category::with('users_cat')->get();
        return view('sell', ['category' => $category]);  
    }

    public function index() {
        $sell = Sell::where('User_id',Auth::user()->id)->with('cat_sell','user_sell')->get();
        return view('list-sell', ['sell' => $sell]);  
    }

    public function edit($id){
        $sell = Sell::with('cat_sell','user_sell')->find($id);
        $category = Category::with('users_cat')->get();
        return view('sell', ['editsell' => $sell, 'category' => $category]);     
    }

    public function store(request $request){
        $request->validate([
            'Product_Image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'Name'=> 'required|max:255',
            'Category_id' => 'required',
            'Product_Price' => 'required|integer',
            'Stock_Product' => 'required|integer',
            'Description' => 'required|max:255',
        ]);
  
        $imageName = str_replace(" ", "_", $request['Name']).'-'.time().'.'.$request['Product_Image']->extension();
        $request['Product_Image']->move(public_path('images/upload-product'), $imageName);

        try{
            $sell = Sell::create([
                'Name' => $request['Name'],
                'Product_Image' => $imageName,
                'Product_Price' => $request['Product_Price'],
                'Stock_Product' => $request['Stock_Product'],
                'Description' => $request['Description'],
                'User_id' => Auth::user()->id,
                'Category_id' => $request['Category_id']
            ]);
            Alert::success('Success', 'Success Add New Product :: '.$request['Name']);
        } catch(QueryException $ex){
            $ex->getMessage();
            Alert::error('Error', 'Message :: '.$ex);
        } 
        
        return redirect('/sell');
    }
    
    public function add_shop()
    {
        $category = Category::with('users_cat')->get();
        $sell = Sell::where('User_id', '!=', Auth::user()->id)->orderBy('created_at','desc')->with('cat_sell','user_sell')->get();
        return view('shop', ['sell' => $sell, 'category' => $category]);
    }

    public function filter_shop(request $request)
    {
        if($request->id_category === "0"){
            return redirect('/shop');
        }
        
        $sell = Sell::where([
                ['Category_id','=', $request->id_category],
                ['User_id', '!=', Auth::user()->id]
                ])->orderBy('created_at','desc')->with('cat_sell','user_sell')->get();

        $category = Category::with('users_cat')->get();
        
        return view('shop', ['sell' => $sell, 'category' => $category, 'id_cat' => $request->id_category]);
    }

    public function single_shop($id)
    {    
        $sell = Sell::find($id);
        //echo "ini single :: ".$id;
        return view('shop-single', ['sell' => $sell]);
    }

    public function update(request $request){
        $request->validate([
            'Product_Image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'Name'=> 'required|max:255',
            'Category_id' => 'required',
            'Product_Price' => 'required|integer',
            'Stock_Product' => 'required|integer',
            'Description' => 'required|max:255',
        ]);

        //echo "ini update :: ".$request->id;
  
        $imageName = str_replace(" ", "_", $request['Name']).'-'.time().'.'.$request['Product_Image']->extension();
        $request['Product_Image']->move(public_path('images/upload-product'), $imageName);

        try{
            $sell = Sell::where('id',$request['id'])->update([
                'Name' => $request['Name'],
                'Product_Image' => $imageName,
                'Product_Price' => $request['Product_Price'],
                'Stock_Product' => $request['Stock_Product'],
                'Description' => $request['Description'],
                'User_id' => Auth::user()->id,
                'Category_id' => $request['Category_id']
            ]);
            Alert::success('Success', 'Success Update Product :: '.$request['Name']);
        } catch(QueryException $ex){
            $ex->getMessage();
            Alert::error('Error', 'Message :: '.$ex);
        } 
        
        return redirect('/sell');
    }

    public function destroy($id){
        //echo "ini id delete ".$id;
        $nameproduk = Sell::find($id);
        try{
            $delete = Sell::where('id',$id)->delete();
            Alert::success('Success', 'Success Delete Product :: '.$nameproduk->Name);
        }catch(QueryException $e){
            $message= $e->getMessage();
            Alert::error('Error', 'Message :: '.$message);
       }
        return redirect('/sell');
        
    }

    public function show(request $request){
        echo 'test';
    }

}
