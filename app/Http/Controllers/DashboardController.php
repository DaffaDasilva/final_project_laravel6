<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sell;
use Alert;

class DashboardController extends Controller
{
    public function index() {
        $sell = Sell::orderBy('Stock_Product','desc')->with('cat_sell','user_sell')->get();
        return view('index', ['sell' => $sell]);
    }


    public function hot_product()
    {
        $sell = Sell::orderBy('Stock_Product','desc')->with('cat_sell','user_sell')->get();
        return view('index', ['sell' => $sell]);
    }
}
