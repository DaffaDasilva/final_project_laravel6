<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Sell;
use App\Beli;
use App\Transaksi;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Alert;
use Session;

class CartController extends Controller
{
    public function index()
    {
        $cart = Beli::where([
                ['users_id',Auth::user()->id],
                ['Status_Pembelian_id','1']
            ])->with('user_beli','sell_beli')->get();
    	return view('cart', ['cart' => $cart]);
    }

    public function countcart(){
        $user_id = Auth::user()->id;

        try{
            $cart = Beli::where([['users_id',$user_id],['Status_Pembelian_id','1']])->get();

            $status = '00';
            $msg = 'Success';
            $data = $cart;
        } catch (QueryException $ex){
            $status = '01';
            $msg = $ex->getMessage();
            $data = [];
        }

        return response()->json([
            'status' => $status,
            'msg' => $msg,
            'data' => $data
        ]);
    }

    public function deltocarts($id){
        $user_id = Auth::user()->id;
        try{
            Beli::where([
                ['Product_id',$id],
                ['users_id',$user_id]
            ])->delete();
            Alert::success('Success', 'Success Delete Product from Cart');
        } catch(QueryException $ex){
            $ex->getMessage();
            Alert::error('Error', $ex);
        }

        return redirect('/cart');
    }

    public function addtocarts(request $request){
        $product_id = $request->id;
        $jumlah_product = $request->jumlah_product;
        $status_pembelian = '1';
        $user_id = Auth::user()->id;  

        $cekbeli = Beli::where([
                ['Product_id',$product_id],
                ['users_id',$user_id],
                ['Status_Pembelian_id',$status_pembelian]
            ])->get();

        $beforejml = 0;
        foreach($cekbeli as $p){
            $beforejml += $p->Jumlah_Product;
        }
        //echo 'ini jumlah product sebelumnya :: '.$beforejml;

        if(count($cekbeli) > 0){
            try{
                Beli::where([
                    ['Product_id',$product_id],
                    ['users_id',$user_id],
                    ['Status_Pembelian_id',$status_pembelian]
                ])->update([
                    'Jumlah_Product' => $beforejml + $jumlah_product
                ]);
                Alert::success('Success', 'Success Add Product to Cart');
            }catch(QueryException $ex){
                $ex->getMessage();
                Alert::error('Error', $ex);
            }
        } else {
            try{
                Beli::create([
                    'Jumlah_Product' => $jumlah_product,
                    'Status_Pembelian_id' => $status_pembelian,
                    'users_id' => $user_id,
                    'Product_id' => $product_id,
                ]);
                Alert::success('Success', 'Success Add Product to Cart');
            }catch(QueryException $ex){
                $ex->getMessage();
                Alert::error('Error', $ex);
            }
        }

        return redirect('/shop-single/'.$product_id);
    }

    public function transaksi(request $request){
        try{
            $cart = Beli::where([
                ['users_id',Auth::user()->id],
                ['Status_Pembelian_id','1']
            ])->update([
                'Status_Pembelian_id' => '2'
            ]);

            try{
                $saldo = User::find(Auth::user()->id);
                $beforesaldo = $saldo->Saldo;

                $user = User::where('id',Auth::user()->id)->update([
                    'Saldo' => $beforesaldo-$request->subtotal
                ]);

                $loops = Beli::where([
                    ['users_id',Auth::user()->id],
                    ['Status_Pembelian_id','2']
                ])->with('user_beli','sell_beli')->get();

                $current_time = Carbon::now('Asia/Jakarta')->toDateTimeString();
        
                foreach($loops as $loop){
                    try{
                        $stock = Sell::find($loop->Product_id);
                        $beforestock = $stock->Stock_Product;
                        $stocksell = Sell::where('id',$loop->Product_id)->update([
                            'Stock_Product' => $beforestock - $loop->Jumlah_Product
                        ]);

                        try{
                            $trans = Transaksi::create([
                                'Order_Date' => $current_time ,
                                'Shipping&Payment_Method' => 'PAID',
                                'Invoice_Amount' => (int)$loop->Jumlah_Product*(int)$loop->sell_beli->Product_Price,
                                'beli_id' => $loop->id
                            ]);
                            Alert::success('Success', 'Success Transactions');
                            Session::flash('suksesTrans','Success Transaction, Happy Shopping Again :).');
                        } catch(QueryException $ext){
                            $ext->getMessage();
                            Alert::error('Error', $ext);
                            Session::flash('gagalTrans',$ext);
                        }
                    } catch(QueryException $exs){
                        $exs->getMessage();
                        Alert::error('Error', $exs);
                        Session::flash('gagalTrans',$exs);
                    }
                }
            } catch(QueryException $exp){
                $exp->getMessage();
                Alert::error('Error', $exp);
                Session::flash('gagalTrans',$exp);
            }
        } catch(QueryException $ex){
            $ex->getMessage();
            Alert::error('Error', $ex);
            Session::flash('gagalTrans',$ex);
        }

        //echo $request->idpembeli.'<br/>';
        //echo $request->subtotal;
        return redirect('shop');
    }
}
