<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Category;
use App\Sell;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Alert;

class CategoryController extends Controller
{
    public function index() {
        $category = Category::with('users_cat')->get();
        return view('category', ['category' => $category]);
        
    }

    public function create() {
        return view('category-create');
    }

    public function store(request $request) {

        $request->validate([
            "Name" => 'required|unique:category',
            "Description" => 'required'
         ]);

        try{
            $sell = Category::create([
                'Name' => $request['Name'],
                'Description' => $request['Description'],
                'users_id' => Auth::user()->id,
            ]);
        } catch(QueryException $a) {
            $message = $a->getMessage();
        }
        Alert::success('Success', 'Success Add New Category :: '.$request['Name']);
        return redirect('/sell/category');
    }
    
    public function sell() {
        $category = Category::all();
        return view('sell', ['category' => $category]);
        
    }
}
