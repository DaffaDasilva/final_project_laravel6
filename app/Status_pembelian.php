<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status_pembelian extends Model
{
    protected $table = "status_pembelian";

    protected $fillable =
    [
        'id',
        'Name',
        'created_at',
        'updated_at',
    ];
}
