<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beli extends Model
{
    protected $table = "beli";

    protected $fillable =
    [
        'Jumlah_Product',
        'created_at',
        'updated_at',
        'Status_Pembelian_id',
        'users_id',
        'Product_id',
    ];

    public function user_beli() {
        return $this->belongsTo('App\User','users_id');
    
    }

    public function sell_beli() {
        return $this->belongsTo('App\Sell','Product_id');
    
    }

    






}
