<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = "category";
    protected $fillable =
        [
            'Name',
            'Description',
            'created_at',
            'updated_at',
            'users_id'
        ];

    public function users_cat() {
        return $this->belongsTo('App\User','users_id');
    }
}
