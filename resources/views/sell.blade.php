@extends('master')

@push('title')
<title>Books Store | Sell</title>
@endpush

@section('content')
<div class="bg-light py-3">
    <div class="container">
        <div class="row">
        	<div class="col-md-12 mb-0"><a href="{{url('/home')}}">Home</a> <span class="mx-2 mb-0">/	</span> <strong class="text-black">Sell</strong><span class="mx-2 mb-0">/	</span><strong class="text-black">@if(isset($editsell)) Edit Product @else Add Product @endif</strong></div>
        </div>
    </div>
</div>
<div class="site-section">
	<div class="container">
		<div class="row">
			<div class="col-md-10">
				<h2 class="h3 mb-3 text-black">Sell Your Book @if(isset($editsell)) Edit :: {{$editsell->Name}}@endif</h2>
			</div>
			<div class="col-md-10">
				<?php
					$urlform = url('/sell');
					if(isset($editsell)){
						$urlform = url('/sell/'.$editsell->id);
					}
				?>
				<form action="{{$urlform}}" method="POST" enctype="multipart/form-data">
					@csrf
					@if(isset($editsell)){{ method_field('PUT') }}
					<input type="hidden" id="id" name="id" required value="{{$editsell->id}}">
					@endif
					<div class="p-3 p-lg-5 border">
						<div class="form-group row">
							<div class="col-md-12">
								<label for="Name" class="text-black">Title<span class="text-danger">*</span></label>
								<input type="text" id="Name" name="Name" required class="form-control @error('Name') is-invalid @enderror" value="@if(isset($editsell)){{$editsell->Name}}@else{{ old('Name') }}@endif" autofocus>
							@error('Name')
                        	<span class="invalid-feedback" role="alert">
                          		<strong>{{ $message }}</strong>
                        	</span>
                      		@enderror
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<label for="Category_id" class="text-black">Category<span class="text-danger">*</span>
									<a href="{{url('/sell/category')}}" class="btn btn-link text-lowercase" style="font-size: 10pt;">Add Category</a></label>
								<select class="form-control @error('Category_id') is-invalid @enderror" id="Category_id" name="Category_id" required>
									<option value="">...</option>
									@foreach($category as $kategori)
										<?php
											$selected = '';
											if(isset($editsell)){
												if($kategori->id == $editsell->Category_id){
													$selected = 'selected="selected"';
												}
											}
										?>
									 	<option value="{{$kategori->id}}" {{$selected}}>{{$kategori->Name}}</option>
									@endforeach
								</select>
								@error('Category_id')
                        		<span class="invalid-feedback" role="alert">
	                          		<strong>{{ $message }}</strong>
                        		</span>
                      			@enderror
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<label for="Product_Price" class="text-black">Price<span class="text-danger">*</span></label>
								<input type="number" id="Product_Price" name="Product_Price" required class="form-control @error('Product_Price') is-invalid @enderror"  value="@if(isset($editsell)){{$editsell->Product_Price}}@else{{ old('Product_Price') }}@endif" autofocus>
								@error('Product_Price')
                        		<span class="invalid-feedback" role="alert">
	                          		<strong>{{ $message }}</strong>
                        		</span>
                      			@enderror
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<label for="Stock_Product" class="text-black">Stock<span class="text-danger">*</span></label>
								<input type="number" id="Stock_Product" name="Stock_Product" required class="form-control @error('Stock_Product') is-invalid @enderror" value="@if(isset($editsell)){{$editsell->Stock_Product}}@else{{ old('Stock_Product') }}@endif" autofocus>
								@error('Stock_Product')
                        		<span class="invalid-feedback" role="alert">
                          			<strong>{{ $message }}</strong>
                        		</span>
                      			@enderror
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<label for="Description" class="text-black">Description</label>
								<textarea name="Description" id="Description" cols="30" rows="7" required class="form-control @error('Description') is-invalid @enderror" autofocus>@if(isset($editsell)){{$editsell->Description}}@else{{ old('Description') }}@endif</textarea>
								@error('Description')
                        		<span class="invalid-feedback" role="alert">
                          			<strong>{{ $message }}</strong>
                        		</span>
                      			@enderror
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="Product_Image" class="text-black">Upload Image</label>
								<input type="file" accept="image/*" name="Product_Image" id="file"  onchange="loadFile(event)" style="display: none;" required autofocus value="@if(isset($editsell)){{$editsell->Product_Image}}@else{{ old('Product_Image') }}@endif">

								<?php
									$src = '';
									$link = '';
									if(isset($editsell)){
										$link = strval($editsell->Product_Image);
									}
									$urlsrc= url("/images/upload-product/".$link);
									if(isset($editsell)){
										$src = 'src='.$urlsrc;
									}
								?>
								<p><img id="output" {{$src}} width="479" height="340"/></p>

								<label for="file" style="cursor: pointer;" class="btn btn-danger">Search</label>
								
								@error('Product_Image')
                        		<span class="invalid-feedback" role="alert">
                          			<strong>{{ $message }}</strong>
                        		</span>
                      			@enderror
							</div>
						</div>
						<div class="form-group row">
							@if(isset($editsell))<div class="col-lg-4"><a href="{{url('/sell')}}" class="btn btn-secondary btn-lg btn-block">Cancel</a></div><div class="col-lg-4"><input type="submit" class="btn btn-primary btn-lg btn-block" value="Update"></div>@else<div class="col-lg-4"><input type="submit" class="btn btn-primary btn-lg btn-block" value="Sell"></div>@endif
						</div>
					</div>
				</form>
			</div>

			<div class="col-md-2 p-3 p-lg-6 border" style="height: 100px;">
				<strong class="text-black">@if(isset($editsell)) Edit Product @else Add Product @endif</strong>
				<br>
				<a href="{{url('/sell')}}">List Product</a>
			</div>
		</div>
	</div>
</div>
@endsection

@push('script')
<script>
	var loadFile = function(event) {
		var image = document.getElementById('output');
		image.src = URL.createObjectURL(event.target.files[0]);
	};
</script>
@endpush