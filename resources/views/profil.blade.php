@extends('master')

@push('title')
<title>Books Store | Profile</title>
@endpush

@section('content')
<div class="bg-light py-3">
	<div class="container">
		<div class="row">
			<div class="col-md-12 mb-0"><a href="{{url('/dashboard')}}">Home</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Profile</strong></div>
		</div>
	</div>
</div>
<div class="site-section">
	<div class="container">
		<div class="row">
			@if ($message = Session::get('topUpSukses'))
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
			@endif
			@if ($message = Session::get('topUpGagal'))
			<div class="alert alert-danger alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
			@endif
			<div class="col-md-12">
				<h2 class="h3 mb-3 text-black">Your Account</h2>
			</div>
			<div class="col-md-12 p-3 p-lg-5 border">
				<div class="container">
					<div class="row">
							<div class="col-sm col-md-3">
								<picture>
								<source srcset="{{asset('images/dp.png')}}" type="multipart/form-data">
									<img src="{{asset('images/foto-profil/'. Illuminate\Support\Facades\Auth::user()->UsersPhoto)}}" width="150px" class="rounded-circle" alt="User Image" >
								</picture>
								<!-- -->	
							</div>
							<div class="col-sm col-md-7">
								<h1>{{Illuminate\Support\Facades\Auth::user()->username}}</h1>
								<h4>{{Illuminate\Support\Facades\Auth::user()->name}}</h4>
							</div>
							<div class="col-sm">
								<h4>Balance : {{Illuminate\Support\Facades\Auth::user()->Saldo}}</h4>
								<button type="button" class="btn btn-default" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Top Up</button>
							</div>
						</div>
					</div>
					<br>
					<div class="col">
						<h6>{{Illuminate\Support\Facades\Auth::user()->Alamat}}</h6>
					</div>
					<a href="{{url('/profile/'.Illuminate\Support\Facades\Auth::user()->id.'/edit')}}" type="button" class="btn btn-link">Edit Profile</a>
				</div>

				<!--HISTORY PEMBELIAN-->
				<div class="col-md-6 p-3 p-lg-5 border">
					<div>
						<h1 class="d-block text-primary h6 text-uppercase">Sales History</h1>
						<table class="table">
							<thead>
								<tr>
									<th scope="col">Sale Order</th>
									<th scope="col">Product Name</th>
									<th scope="col">Category</th>
									<th scope="col">Qty</th>
									<th scope="col">Status</th>
									<th scope="col">Invoice Amount</th>
								</tr>
							</thead>
							<tbody>
								@foreach($saleHistory as $index)
								<tr>
									<td>{{$index->Order_Date}}</td>
									<td>{{$index->Product_name}}</td>
									<td>{{$index->Category_name}}</td>
									<td>{{$index->Jumlah_Product}}</td>
									<td>{{$index->status_paid}}</td>
									<td>{{$index->Invoice_Amount}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>

				<!--HISTORY PENJUALAN-->
				<div class="col-md-6 p-3 p-lg-5 border">	
					<div>
						<h1 class="d-block text-primary h6 text-uppercase">Purchase History</h1>
						<table class="table">
							<thead>
								<tr>
									<th scope="col">Order date</th>
									<th scope="col">Category</th>
									<th scope="col">Product Name</th>
									<th scope="col">Qty</th>
									<th scope="col">Total Price</th>
								</tr>
							</thead>
							<tbody>
								@foreach($buy as $z)
								<?php
									$catName = '-';
									foreach($relasicategory as $sel){
										if($sel->cat_sell->id === $z->sell_beli->Category_id){
											$catName = $sel->cat_sell->Name;
										}
									}
								?>

								<tr>
									<td>{{$z->updated_at}}</td>
									<td>{{$catName}}</td>
									<td>{{$z->sell_beli->Name}}</td>
									<td>{{$z->Jumlah_Product}}</td>
									<td>{{(int)$z->Jumlah_Product*(int)$z->sell_beli->Product_Price}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<!--TOP UP-->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-Index:99999">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Top Up Balance</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form action="{{url('/topupsaldo')}}" method='POST'>
							@csrf
							<div class="form-group">
								<label for="Saldo" class="col-form-label">Balance</label>
								<input type="number" id="Saldo" name="Saldo" class="form-control">
							</div>
							<div class="password">
								<label for="message-text" class="col-form-label">Your Password Account</label>
								<input type="password" id="password" name="password" class="form-control">
							</div>

							<div class="modal-footer">
								<input type="submit" value="TopUp" class="btn btn-primary">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

@endsection