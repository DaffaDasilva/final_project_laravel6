
@extends('master')

@push('title')
<title>Books Store | List Sells</title>
@endpush

@section('content')
<div class="bg-light py-3">
    <div class="container">
        <div class="row">
        	<div class="col-md-12 mb-0"><a href="{{url('/home')}}">Home</a> <span class="mx-2 mb-0">/	</span> <a href="{{url('/sell/create')}}">Sell</a><span class="mx-2 mb-0">/</span><strong class="text-black">List Product</strong></div>
        </div>
    </div>
</div>
<div class="site-section">
	<div class="container">
		<div class="row">
			<div class="col-md-10">
				<h2 class="h3 mb-3 text-black">Your Sell Products</h2>
			</div>
			<div class="col-md-10">
				<table id="example1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th></th>
						<th>Title</th>
						<th>Categories</th>
						<th>Description</th>
						<th>Upload by</th>
						<th>Price</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				  @if(count($sell) > 0)
				  	@foreach($sell as $index)
						<tr>
							<td>
								<img src="{{url('/images/upload-product/'. $index->Product_Image)}}" alt="Product" height="auto" width="150px"/>
							</td>
							<td>{{$index->Name}}</td>
							<td>{{$index->cat_sell->Name}}</td>
							<td>{{$index->Description}}</td>
							<td>{{$index->user_sell->username}}</td>
							<td>{{$index->Product_Price}}</td>
							<td width="200px" class="text-center">
									<?php
										$url='/sell/'.$index->id;
										$urledit='/sell/'.$index->id.'/edit';
									?>
									<form class="form-group" action="{{url($url)}}" method="POST" id="formdelete">
									@csrf
									{{ method_field('DELETE') }}
										<a href="#" class="btn btn-secondary btn-sm showSell" data-sell="{{$index}}" data-toggle="modal" data-target="#modalShowSell" data-whatever="@mdo">
										<i class="icon icon-eye"></i>
										</a>
										<a href="{{url($urledit)}}" class="btn btn-primary btn-sm">
										<i class="icon icon-pencil"></i>
										</a>
										<a href="#" class="btn btn-danger btn-sm deleteSell" data-id="{{$index->id}}"><i class="icon icon-trash"></i></a>	
									</form>
							</td>
						</tr>
				  @endforeach
				  @else
				  	<tr>
						<td colspan="7" class="text-center"><span class="icon icon-file"></span><br/> No data available.</td>
					</tr>
				  @endif
				</tbody>
				</table>
			</div>
			<div class="col-md-2 p-3 p-lg-6 border position-sticky" style="height: 100px; position: sticky">
				<a href="{{url('/sell/create')}}">Add Product</a>
				<br>
				<strong class="text-black">List Product</strong>
			</div>
		</div>
	</div>	
	<!--TOP UP-->
	<div class="modal fade" id="modalShowSell" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-Index:99999">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="titlesell">Detail Product</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="{{url('/topupsaldo')}}" method='POST'>
						@csrf
						<div class="form-group">
							<label for="product_image" class="col-form-label">Product Image</label>
							<div class="w-100 text-center" id="imgsell">
							<img id="sellimg" src="{{ url('/images/upload-product') }}" width="350px" height="auto" alt="Image Product"/></div>
						</div>
						<div class="">
							<label for="name" class="col-form-label">Category</label>
							<input type="text" id="categorysell" readonly name="categorysell" class="form-control">
						</div>
						<div class="">
							<label for="name" class="col-form-label">Name</label>
							<input type="text" id="namesell" readonly name="namesell" class="form-control">
						</div>
						<div class="">
							<label for="name" class="col-form-label">Product Price</label>
							<input type="text" id="pricesell" readonly name="pricesell" class="form-control">
						</div>
						<div class="">
							<label for="name" class="col-form-label">Stock Product</label>
							<input type="text" id="stocksell" readonly name="stocksell" class="form-control">
						</div>
						<div class="">
							<label for="name" class="col-form-label">Description</label>
							<textarea row="5" id="descsell" readonly name="descsell" class="form-control"></textarea>
						</div>
						<div class="">
							<label for="name" class="col-form-label">Created By</label>
							<input type="text" id="createbysell" readonly name="createbysell" class="form-control">
						</div>
						<div class="">
							<label for="name" class="col-form-label">Created At</label>
							<input type="text" id="createdatsell" readonly name="createdatsell" class="form-control">
						</div>
						<div class="">
							<label for="name" class="col-form-label">Updated At</label>
							<input type="text" id="updatedatsell" readonly name="updatedatsell" class="form-control">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@push('script')
<script type='text/javascript'>
 $('.deleteSell').click(function () {
        var postId = $(this).data('id'); 
		var result = confirm("Want to delete?");
		if (result) {
			//Logic to delete the item
			$("#formdelete").submit();
		}	
});

$('.showSell').click(function (){
	var post = $(this).data('sell');
	//alert(JSON.stringify(post));
	//alert($("#sellimg").attr('src'));
	$imgsell = "<?php echo url('/images/upload-product') ?>";
	//console.log($imgsell);
	$("#titlesell").text('Detail Product :: '+post.Name);
	$("#sellimg").attr('src', $imgsell+'/'+post.Product_Image);
	$("#namesell").val(post.Name);
	$("#descsell").val(post.Description);
	$("#pricesell").val(post.Product_Price);
	$("#stocksell").val(post.Stock_Product);
	$("#createbysell").val(post.user_sell.username);
	$("#createdatsell").val(post.created_at);
	$("#updatedatsell").val(post.updated_at);
	$("#categorysell").val(post.cat_sell.Name);
});
</script>
@endpush
@endsection

