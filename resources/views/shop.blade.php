@extends('master')

@push('title')
<title>Books Store | Shop</title>
@endpush

@section('content')
<div class="bg-light py-3">
	<div class="container">
		<div class="row">
			<div class="col-md-12 mb-0"><a href="{{url('/home')}}">Home</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Shop</strong></div>
		</div>
	</div>
</div>
<div class="site-section">
	<div class="container">
		@if ($message = Session::get('suksesTrans'))
			<div class="alert alert-success alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
			@endif
			@if ($message = Session::get('gagalTrans'))
			<div class="alert alert-danger alert-block">
				<button type="button" class="close" data-dismiss="alert">×</button> 
				<strong>{{ $message }}</strong>
			</div>
		@endif
		<div class="row mb-5">
			<div class="col-md-12 order-2">

				<div class="row">
					<div class="col-md-12 mb-5">
						<div class="float-md-left mb-4"><h2 class="text-black h5">Shop All</h2></div>
					</div>
				</div>
				<form method="post" action="{{url('/shop/filter')}}">
					<div class="row mb-5">
					@csrf
						<div class="col-sm-2">
							<label class="form-label">Filter by category</label>
						</div>
						<div class="col-sm-3">
							<select name="id_category" class="form-control">
								<option value="0">-- ALL --</option>
								@foreach($category as $cat)
								<?php
									$selected = '';
									if(isset($id_cat)){
										if($cat->id == $id_cat){
											$selected = 'selected="selected"';
										}
									}
								?>
									<option value="{{$cat->id}}" {{ $selected }}>{{$cat->Name}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-sm-3">
							<button type="submit" class="btn btn-primary">Search</button>
						</div>
					</div>
				</form>
				<div class="row mb-5">
					@foreach($sell as $shop)
					<div class="col-sm-6 col-lg-4 mb-4" data-aos="fade-up">
						<div class="block-4 text-center border">
							<figure class="block-4-image">
								<a href="{{url('/shop-single/'.$shop->id)}}">
									<img src="{{url('/images/upload-product/'. $shop->Product_Image)}}" alt="Product Image" class="img-fluid" widh="200px" height="350px">
								</a>
							</figure>
							<div class="block-4-text p-4">
								<h3><a href="{{url('/shop-single/'.$shop->id)}}">{{$shop->Name}}</a></h3>
								<p class="mb-0">{{$shop->Description}}</p>
								<p class="text-primary font-weight-bold">{{$shop->Product_Price}}</p>
								<small class="text-primary font-weight-bold">Category : {{$shop->cat_sell->Name}}</small>
							</div>
						</div>
					</div>
					@endforeach
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection