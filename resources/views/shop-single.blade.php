@extends('master')

@push('title')
<title>Books Store | </title>
@endpush

@section('content')
    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"><a href="{{url('/home')}}">Home</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">{{$sell->Name}}</strong></div>
        </div>
      </div>
    </div>  

    <div class="site-section">
      <div class="container">
        <form method="POST" action="{{url('/addtocart')}}" id="formCart" enctype="multipart/form-data">
          @csrf
          <input type="hidden" name="id" value="{{$sell->id}}">
          <div class="row">
            <div class="col-md-6">
              <img src="{{url('/images/upload-product/'.$sell->Product_Image)}}" alt="Image" class="img-fluid" height="auto" width="550px">
            </div>
            <div class="col-md-6">
              <h2 class="text-black">{{$sell->Name}}</h2>
              <p>Categories : {{$sell->cat_sell->Name}}</p>
              <p class="mb-4">Description : {{$sell->Description}}</p>
              <p><strong class="text-primary h4">Rp.{{$sell->Product_Price}},-</strong></p>
              <div class="mb-5 w-100">
                <div class="input-group mb-3" style="max-width: 200px;">
                <div class="input-group-prepend">
                  <button class="btn btn-outline-primary js-btn-minus" type="button">&minus;</button>
                </div>
                <input type="number" class="form-control text-center" value="1" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1" id="stockSell" name="jumlah_product" max="{{$sell->Stock_Product}}">
                <div class="input-group-append">
                  <button class="btn btn-outline-primary js-btn-plus" type="button">&plus;</button>
                </div>
              </div>

              </div>
              <p style="max-width: 200px;"><button type="button" id="btn-submit" class="buy-now btn btn-sm btn-primary w-100">Add To Cart</a></p>

            </div>
          </div>
        </form>
      </div>
    </div>

@endsection

@push('script')
  <script type="text/javascript">
  $( document ).ready(function() {
    setInterval(function(){
      var stockval = $("#stockSell").val();
      var stock = $("#stockSell").attr('max');
      if(parseInt(stockval) > parseInt(stock)){
        $("#stockSell").val(stock);
      }
    }, 100);
  });

  $('#btn-submit').click(function () {
    var stockvalue = $("#stockSell").val();
		if (stockvalue > 0) {
			//Logic to delete the item
			$("#formCart").submit();
		}	
});
  </script>
@endpush