@extends('master')

@push('title')
<title>Books Store | Edit Profile</title>
@endpush

@section('content')
<div class="site-section">
	<div class="container">
		<div class="row">
			<div class="col-md-10">
				<h2 class="h3 mb-3 text-black">Edit Profile</h2>
			</div>
			<div class="col-md-10">
				<form method="POST" action="{{url('/profile/'.Illuminate\Support\Facades\Auth::user()->id)}}" enctype="multipart/form-data" >
					@csrf
					<!--@method('PUT')-->
					<div class="form-group row">
						<div class="col-md-6">
							<label for="UsersPhoto" class="text-black">Upload Image</label>
							<input type="file" accept="image/*" name="UsersPhoto" id="file"  onchange="loadFile(event)" style="display: none;" autofocus required>
							<p><picture>
								<source srcset="{{asset('images/dp.png')}}" type="multipart/form-data">
									<img id="output" src="{{url('/images/foto-profil/'. Illuminate\Support\Facades\Auth::user()->UsersPhoto)}}" width="150px" class="rounded-circle" alt="User Image" >
								</picture></p>

							<label for="file" style="cursor: pointer;" class="btn btn-danger">Search</label>

							@error('UsersPhoto')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
							@enderror
						</div>
					</div>
					<div class="form-group">
						<label for="name" class="text-black">Name</label>
						<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', $update->name) }}" required autocomplete="name" autofocus placeholder="Name">

						@error('name')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>

					<div class="form-group">
						<label for="username" class="text-black">Username</label>
						<input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username', $update->username) }}" required autocomplete="username" autofocus placeholder="Username">

						@error('username')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>

					<div class="form-group">
						<label for="Alamar" class="text-black">Address</label>
						<input id="Alamat" type="text" class="form-control @error('Alamat') is-invalid @enderror" name="Alamat" value="{{ old('Alamat', $update->Alamat) }}" required autocomplete="Alamat" autofocus  placeholder="Alamat">

						@error('Alamat')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>

					<div class="form-group">
						<input id="saldo" type="hidden" class="form-control @error('saldo') is-invalid @enderror" name="Saldo" value="0" required autocomplete="saldo" autofocus>

						@error('saldo')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>

					<div class="form-group">
						<label for="email" class="text-black">Email</label>
						<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email', $update->email) }}" required autocomplete="email" placeholder="Email Address">

						@error('email')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>

					<div class="form-group row">
						<div class="col-sm-6 mb-3 mb-sm-0">
							<label for="password" class="text-black">Password</label>
							<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password">

							@error('password')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
							@enderror
						</div>

						<div class="col-sm-6">
							<label for="password_confirmation" class="text-black">Repeat Password</label>
							<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Repeat Password">
						</div>

					</div>
					<div>
						<button type="input" class="btn btn-primary">
							{{ __('Update') }}
						</button>
					</div>
					<hr>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@push('script')
<script>
	var loadFile = function(event) {
		var image = document.getElementById('output');
		image.src = URL.createObjectURL(event.target.files[0]);
	};
</script>
@endpush