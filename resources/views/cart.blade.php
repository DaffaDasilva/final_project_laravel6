@extends('master')

@push('title')
<title>Books Store | Cart</title>
@endpush

@section('content')
<div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"><a href="{{url('/dashboard')}}">Home</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Cart</strong></div>
        </div>
      </div>
    </div> 
<div class="site-section">
      <div class="container">
        <div class="row mb-5">
          <form class="col-md-12" method="post">
            <div class="site-blocks-table">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th class="product-thumbnail">Image</th>
                    <th class="product-name">Product</th>
                    <th class="product-price">Price</th>
                    <th class="product-quantity">Quantity</th>
                    <th class="product-total">Total</th>
                    <th class="product-remove">Remove</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  $subtotal = 0;
                  $idpembeli = '';
                  $idcart = array();
                ?>

                @foreach($cart as $p)
                  <?php
                    $total = (int)$p->sell_beli->Product_Price*(int)$p->Jumlah_Product;
                    $subtotal += $total;
                    $idpembeli = $p->user_beli->id;
                    array_push($idcart,$p->id);
                  ?>
                    
                  <tr>
                    <td class="product-thumbnail">
                      <img src="{{url('/images/upload-product/'.$p->sell_beli->Product_Image)}}" alt="Image" class="img-fluid">
                    </td>
                    <td class="product-name">
                      <h2 class="h5 text-black">{{$p->sell_beli->Name}}</h2>
                    </td>
                    <td>Rp.{{$p->sell_beli->Product_Price}},-</td>
                    <td>{{$p->Jumlah_Product}}</td>
                    <td>Rp.{{(int)$p->sell_beli->Product_Price*(int)$p->Jumlah_Product}},-</td>
                    <td>
                    <?php
                      $urldeletecart = '/cart/'.$p->sell_beli->id.'/delete';
                    ?>
										<a href="{{url($urldeletecart)}}" class="btn btn-danger btn-sm deleteCart" data-id="{{$p->sell_beli->id}}">X</a>
                    </td>
                  </tr>
                
                  @endforeach

                  @if(count($cart) === 0)
                  <tr>
                    <td class="product-name text-center" colspan="6">
                      <h2 class="h5 text-black">No data available</h2>
                    </td>
                  </tr>
                  @endif

                </tbody>
              </table>
            </div>
          </form>
        </div>

        <form action="{{url('/post-transaksi')}}" method="post" id="myTransaksi" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="idpembeli" value="{{$idpembeli}}">
            <input type="hidden" name="subtotal" value="{{$subtotal}}">
        </form>

        <div class="row">
          <div class="col-md-6">
            <div class="row mb-5">
              <div class="col-md-6">
                <a href="{{url('/shop')}}" class="btn btn-outline-primary btn-sm btn-block">Continue Shopping</a>
              </div>
            </div>
            <div class="row">
            </div>
          </div>
          <div class="col-md-6 pl-5">
            <div class="row justify-content-end">
              <div class="col-md-7">
                <div class="row">
                  <div class="col-md-12 text-right border-bottom mb-5">
                    <h3 class="text-black h4 text-uppercase">Cart Totals</h3>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-md-6">
                    <span class="text-black">Subtotal</span>
                  </div>
                  <div class="col-md-6 text-right">
                    <strong class="text-black">Rp.{{$subtotal}},-</strong>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-md-6">
                    <span class="text-black">Total</span>
                  </div>
                  <div class="col-md-6 text-right">
                    <strong class="text-black">Rp.{{$subtotal}},-</strong>
                  </div>
                </div>
                <div class="row mb-5">
                  <div class="col-md-6">
                    <span class="text-black">Your Balance</span>
                  </div>
                  <div class="col-md-6 text-right">
                    <strong class="text-black">Rp.{{Illuminate\Support\Facades\Auth::user()->Saldo}},-</strong>
                  </div>
                </div>
                <input type="hidden" id="saldoA" value="{{Illuminate\Support\Facades\Auth::user()->Saldo}}">
                <input type="hidden" id="totalA" value="{{$subtotal}}">
                <div class="row">
                  <div class="col-md-12">
                    <button class="btn btn-primary btn-lg py-3 btn-block" id="prosesSell">Proceed To Checkout</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@push('script')
<script type='text/javascript'>
 $('#prosesSell').click(function () {
    var totalA = $("#totalA").val(); 
		var saldoA = $("#saldoA").val();
		if (parseInt(totalA) > parseInt(saldoA)) {
      alert('Please fill your cash balance');
		}	else {
      $("#myTransaksi").submit();
    }
});
</script>
@endpush