<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index', 301);

Route::get('/dashboard', 'DashboardController@hot_product')->name('dashboard');

Route::get('/login', function(){
	return view('login');
});

Route::get('/register', function(){
	return view('register');
});

Auth::routes();


Route::group(['middleware' => 'auth'], function(){
    Route::get('/home','DashboardController@hot_product')->name('home');
    //PROFIL
    Route::get('/profile','ProfileController@index')->name('profile');

    Route::resource('/sell/category', 'CategoryController');

    Route::POST('/topupsaldo','ProfileController@topUpSaldo')->name('topupsaldo');

    //DAFTAR BUKU YANG DIJUAL
    Route::get('/shop', 'SellController@add_shop');

    Route::post('/shop/filter', 'SellController@filter_shop');

    //USER JUAL BUKU
    Route::resource('/sell','SellController');

    //BUKU YANG ADA DI KERANJANG
    Route::get('/cart', 'CartController@index');

    //CHEHKOUT
    Route::get('/checkout', function(){
	    return view('checkout');
    });

    //LAMAN BUKU PER ITEM
    Route::get('/shop-single/{id}', 'SellController@single_shop');

    Route::get('/profile/{id}', 'ProfileController@purchaseHistory');
    
    Route::get('/profile/{id}/edit', 'ProfileController@edit')->name('edit');

    Route::post('/profile/{id}','ProfileController@updateProfile')->name('update-profile');
    
    Route::post('/addtocart', 'CartController@addtocarts');

    Route::post('/post-transaksi', 'CartController@transaksi');

    Route::get('/cart/{id}/delete', 'CartController@deltocarts');

    Route::get('get-chart-count', 'CartController@countcart');
});
