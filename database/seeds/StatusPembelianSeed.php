<?php

use Illuminate\Database\Seeder;

class StatusPembelianSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $arr = [
            [
                'id'=>'1',
                'Name'=>'Belum Bayar',
                'updated_at'=>date('Y-m-d h:i:s'),
                'created_at'=>date('Y-m-d h:i:s'),
            ],[
                'id'=>'2',
                'Name'=>'Sudah Bayar',
                'updated_at'=>date('Y-m-d h:i:s'),
                'created_at'=>date('Y-m-d h:i:s'),
            ],
        ];
        DB::table('status_pembelian')->insert($arr);
    }
}
