<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusPembelianIdToBeliTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('beli', function (Blueprint $table) {
            $table->unsignedBigInteger('Status_Pembelian_id');
            $table->foreign('Status_Pembelian_id')->references('id')->on('status_pembelian');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('beli', function (Blueprint $table) {
            //
        });
    }
}
